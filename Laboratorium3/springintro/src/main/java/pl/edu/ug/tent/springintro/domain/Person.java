package pl.edu.ug.tent.springintro.domain;

public class Person {

  private String name;

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  private String surname;

  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }

  private String email;
  private String username;

  public Person() {
    System.out.println(this);
  }

  public Person(String name, String surname, String email, String username) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.username = username;
    System.out.println(this);
  }

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name + " " + surname + " " + email + " " + username + "\n";
  }
}
