package pl.edu.ug.tent.springintro.domain;

public class Dziecko extends Person {
    public Ojciec getStary() {
        return stary;
    }
    public void setStary(Ojciec stary) {
        this.stary = stary;
    }
    public Matka getStara() {
        return stara;
    }
    public void setStara(Matka stara) {
        this.stara = stara;
    }

    private Ojciec stary;
    private Matka stara;

    public Dziecko(String name, String surname, int yob) {
        super(name, surname, yob);
    }

    public Dziecko(String n, String s, int y, Ojciec stary, Matka stara) {
        super(n, s, y);
        this.stara = stara;
        this.stary = stary;
    }
    public Dziecko() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return super.toString() + " ojciec dziecka: " + stary.getName() + "\nmatka dziecka: " + stara.getName();
    }
}
