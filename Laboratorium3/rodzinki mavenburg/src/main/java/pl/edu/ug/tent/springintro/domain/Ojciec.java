package pl.edu.ug.tent.springintro.domain;

public class Ojciec extends Person {
    public Dziecko getMlode() {
        return mlode;
    }
    public void setMlode(Dziecko mlode) {
        this.mlode = mlode;
    }

    private Dziecko mlode;

    public Ojciec() {
        System.out.println(this);
    }

    public Ojciec(String n, String s, int y, Dziecko mlode) {
        super(n, s, y);
        this.mlode = mlode;
    }

    @Override
    public String toString() {
        return super.toString() + " dziecko: " + mlode.getName();
    }
}
