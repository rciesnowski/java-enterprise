package pl.edu.ug.tent.springintro.domain;

public class Rodzina {
    private Dziecko d;
    private Matka m;
    private Ojciec o;

    public Rodzina(String surname, String nO, int yO, String nM, int yM, String nD, int yD) {
        this.d = new Dziecko(nD, surname, yD);
        this.o = new Ojciec(nO, surname, yO, this.d);
        this.m = new Matka(nM, surname, yM, this.d);
        this.d.setStara(this.m);
        this.d.setStary(this.o);
        System.out.print(this);
    }
    @Override
    public String toString() {
        return "oto rodzinka " + o.getSurname() +
                ":\n\tdziecko: " + d.getName() + " ur. " + d.getYob() +
                "\n\tmatka: " + m.getName() + " ur. " + m.getYob() +
                "\n\tociec: " + o.getName() + " ur. " + o.getYob() + "\n";
    }
}
