package pl.edu.ug.tent.springintro.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.ug.tent.springintro.domain.*;

@Configuration
public class RodzinaServiceConfig {

  @Bean
  public Rodzina ziomalek() {
    return new Rodzina("Ziomalek", "Henryk", 1965, "Zofia", 1962, "Pawełek", 1988);
  }
}
