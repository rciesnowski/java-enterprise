package pl.edu.ug.tent.springintro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.ug.tent.springintro.domain.Person;
import pl.edu.ug.tent.springintro.domain.Rodzina;

@Service
public class RodzinaService {

  @Autowired
  @Qualifier("ziomalek")
  Rodzina ziomalek;
  Rodzina getZiomalek(){
    return ziomalek;
  }

}
