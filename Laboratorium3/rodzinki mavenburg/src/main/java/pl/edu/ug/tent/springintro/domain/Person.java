package pl.edu.ug.tent.springintro.domain;

public class Person {

  private String name;
  private String surname;
  private int yob;

  public Person() {
    System.out.println(this);
  }

  public Person(String name, String surname, int yob) {
    this.name = name;
    this.surname = surname;
    this.yob = yob;
  }

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getSurname() {
    return surname;
  }
  public void setSurname(String surname) {
    this.surname = surname;
  }
  public int getYob() {
    return yob;
  }
  public void setYob(int yob) {
    this.yob = yob;
  }

  @Override
  public String toString() {
    return name + " " + surname + " " + yob;
  }
}
