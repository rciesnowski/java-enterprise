package pl.edu.ug.tent.springintro.domain;

public class Matka extends Person {
    private Dziecko mlode;

    public Matka() {
        System.out.println(this);
    }
    public Matka(String n, String s, int y, Dziecko mlode) {
        super(n, s, y);
        this.mlode = mlode;
    }

    public Dziecko getMlode() {
        return mlode;
    }
    public void setMlode(Dziecko mlode) {
        this.mlode = mlode;
    }

    @Override
    public String toString() {
        return super.toString() + "dziecko: " + mlode.getName();
    }
}
