package com.example.ProjektLombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:convertcsv.xml")
@SpringBootApplication
public class ProjektLombokApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektLombokApplication.class, args);
	}

}
