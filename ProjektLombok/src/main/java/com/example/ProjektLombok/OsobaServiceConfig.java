package com.example.ProjektLombok;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.ProjektLombok.*;

@Configuration
public class OsobaServiceConfig {

    @Bean
    public Osoba dodajOsobe() {
        return new Osoba(1, "a", "b", "b", "c", "ha");
    }
}