package com.example.ProjektLombok;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Firma {
    private String nazwa;
    private Osoba prezes;
}
