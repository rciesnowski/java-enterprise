package com.example.ProjektLombok;

import lombok.*;

@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Osoba {
    private int id;
    private String imie, nazwisko, email, firma, data;
}
