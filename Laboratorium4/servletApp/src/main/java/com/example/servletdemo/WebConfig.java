package com.example.servletdemo;
import javax.servlet.http.HttpServlet;

import com.example.servletdemo.Web.Witaj;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {
    @Bean
    public ServletRegistrationBean<HttpServlet> GetterPosterSevlet() {

        ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean<>();

        servRegBean.setServlet(new Witaj());
        servRegBean.addUrlMappings("/*");
        servRegBean.setLoadOnStartup(1);

        return servRegBean;

    }
}