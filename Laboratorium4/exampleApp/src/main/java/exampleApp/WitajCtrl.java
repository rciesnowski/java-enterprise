package exampleApp;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WitajCtrl {
  @GetMapping("/w")
  public String witaj(Model model, @RequestParam(value="name", required=false, defaultValue="świecie") String name) {
    model.addAttribute("name", name);
    return "witaj";
  }
}
