package exampleApp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller            // <1>
public class FormCtrl {

  @GetMapping("/f")     // <2>
  public String form() {
    return "formularz";     // <3>
  }

}
